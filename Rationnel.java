public class Rationnel {
	private long numerateur;
	private long denominateur;
	
	public static long pgcd (long a, long b) { // 2 parametres en entree, en retourne 1 (procedure)
		while (a != b) {					   // Modifie des parametres
			if (a > b) {					   // Methode de type modificateur (public static) retournant un entier long (et 2 en entree) 
				a -= b;
			} else {
				b -= a;
			}
		}
		return a;
	}
	
	public Rationnel (long numerateur, long denominateur) // 2 parametres en entree et sortie (procedure)
			throws IllegalArgumentException {			  // Affecte des parametres 
		if (denominateur == 0) {						  // Methode de type constructeur (public) retournant un rationnel (2 entiers entree)
			throw new IllegalArgumentException ();
		}
		this.numerateur = numerateur;
		this.denominateur = denominateur;
	}

	public long getNumerateur () { // Retourne 1 parametre (fonction), sort une valeur, methode de type observateur (public) 
		return this.numerateur;    // Retournant un entier long
	}
	
	public long getDenominateur () { // Retourne 1 parametre (fonction), sort une valeur, methode de type observateur (public) 
		return this.denominateur;    // Retournant un entier long
	}
	
	public Rationnel reduction () {							  // Retourne 1 parametre (fonction)
		long pgcd = pgcd(this.numerateur, this.denominateur); // Modifie un parametre 
		return new Rationnel (this.numerateur/pgcd,			  // Methode de type modificateur (public) retournant un rationnel
				this.denominateur/pgcd);
	}
	
	public Rationnel somme (Rationnel r) { 							// 1 parametre en entree, en retourne 1 (fonction)
		return new Rationnel (this.numerateur*r.denominateur		// Cree un nouveau parametre
				+r.numerateur*this.denominateur, this.denominateur* // Methode de type evaluateur (public) retournant un rationnel
				r.denominateur);
	}
	
	public Rationnel produit (Rationnel r) { 				// 1 parametre en entree, en retourne 1 (fonction)
		return new Rationnel (this.numerateur*r.numerateur, // Cree un nouveau parametre
				this.denominateur*r.denominateur);			// Methode de type evaluateur (public) retournant un rationnel
	}
	
	public String toString () { 										// retourne 1 parametre (fonction)
		Rationnel irreductible = this.reduction(); 						// Modifie un parametre
		return (irreductible.numerateur+"/"+irreductible.denominateur); // Methode de type modificateur (public) retournant une phrase
	}
}
